//
//  PublicData.swift
//  PhotoStorage
//
//  Created by georg syncov on 01/03/2019.
//  Copyright © 2019 georg syncov. All rights reserved.
//

import Foundation



import Foundation
import UIKit

class MyUserData: Codable {
    
    static let manager = MyUserData()
    private init(){}
    
    
    var myUserImage : UIImage?
    var myUserDescription : String?
    
    init(myUserImage : UIImage, myUserDescription: String) {
        self.myUserImage = myUserImage
        self.myUserDescription = myUserDescription
    }
    
    func getMyUserImage() -> UIImage? {
        return myUserImage ?? UIImage(named: "min")
    }
    
    func getMyUserDescription() -> String? {
        return myUserDescription ?? "Description of MyUserImage"
    }
    
    func setMyUserImage(_ myUserImage: UIImage) {
        self.myUserImage = myUserImage
    }
    
    func setMyUserDescription(_ myUserDescription: String) {
        self.myUserDescription = myUserDescription
    }
    
    public enum CodingKeys: String, CodingKey{
        case myUserImage, myUserDescription
    }
    
    required public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.myUserDescription = try container.decodeIfPresent(String.self, forKey: .myUserDescription) ?? ""
        
        let data = try container.decodeIfPresent(Data.self, forKey: .myUserImage)
        if let data = data {
            guard let photo = UIImage(data: data) else {return}
            self.myUserImage = photo
        } else {
            myUserImage = nil
        }
    }
    
    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        if let myUserImage = myUserImage {
            guard let data = myUserImage.jpegData(compressionQuality: 1)
                else {
                    return
            }
            try container.encode(data, forKey: .myUserImage)
        }
   
        try container.encode(self.myUserDescription, forKey: .myUserDescription)
 
    }
}

