//
//  ViewPhotoCollectionItem.swift
//  PhotoStorage
//
//  Created by georg syncov on 28/02/2019.
//  Copyright © 2019 georg syncov. All rights reserved.
//

import UIKit

class ViewPhotoCollectionItem: UIView {

    @IBOutlet weak var photoImageViewInItem: UIImageView!
    
    @IBOutlet weak var descriptionLabelInItem: DescriptionLabel!
    
    func setUserImage(_ userImage: UIImage){
        photoImageViewInItem.image = userImage
        photoImageViewInItem.contentMode = .scaleAspectFit
    }

    func setUserDescription(_ userDescription: String? = ""){
        descriptionLabelInItem.text = userDescription
    }
}

extension UIView {
    static func instanceFromNib() -> UIView {
        return UINib(nibName: "\(self)", bundle: nil).instantiate(withOwner: nil, options: nil).first as! UIView
    }
}

extension UIColor {
    var contastingTextColour: UIColor {
        
        var brightness: CGFloat = 0
        getHue(nil, saturation: nil, brightness: &brightness, alpha: nil)
        
        return brightness < 0.5 ? .white : .black
    }
}

