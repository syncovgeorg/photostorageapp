//
//  PhotoCollectionViewCell.swift
//  PhotoStorage
//
//  Created by georg syncov on 27/02/2019.
//  Copyright © 2019 georg syncov. All rights reserved.
//

import UIKit

class PhotoCollectionViewCell: UICollectionViewCell {
    
    let mainImageView: ViewPhotoCollectionItem = {
        let view = ViewPhotoCollectionItem.instanceFromNib() as! ViewPhotoCollectionItem
        return view
    }()
    
    func setUserDataOnPhotoView(_ userData: MyUserData){
        guard let image = userData.getMyUserImage() else {
            return
        }
        mainImageView.setUserImage(image)
        mainImageView.setUserDescription(userData.getMyUserDescription())
        mainImageView.frame.size = self.frame.size
        mainImageView.frame.origin = CGPoint.zero
        self.addSubview(mainImageView)
    }
    
    override func layoutSubviews() {
        self.layer.cornerRadius = 5
        self.layer.shadowRadius = 9
        layer.shadowOpacity = 0.3
        layer.shadowOffset = CGSize(width: 5, height: 8)
        self.clipsToBounds = false
        
    }
    

}
