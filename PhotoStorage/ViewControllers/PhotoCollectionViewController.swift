



import UIKit

class PhotoCollectionViewController: UIViewController {
    
    @IBOutlet weak var photoCollectionView: UICollectionView!
    @IBOutlet weak var enableEditingButton: MainButton!
    @IBOutlet weak var addImageButton: MainButton!
    
    @IBOutlet weak var togleMenuButton: UIButton!
    
    let imagePicker = UIImagePickerController()
    var sideMenuViewController = SideMenuViewController()
    static var isUserPasswordModeChanged = Bool()
   
    var myPhotos = {PhotoManager.shared.getPhotos() ?? [MyUserData]()}()
    var isEditingEnabled = Bool(){
        didSet {
            if isEditingEnabled{
                enableEditingButton.setTitle("Editing enable", for: .normal)
                enableEditingButton.backgroundColor = .green
                enableEditingButton.setTitleColor(UIColor.black, for: .normal)
            } else {
                enableEditingButton.backgroundColor = .lightGray
                enableEditingButton.setTitle("Enable editing?", for: .normal)
                enableEditingButton.setTitleColor(UIColor.blue, for: .normal)
            }
            
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        imagePicker.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        isEditingEnabled = false
        PhotoCollectionViewController.isUserPasswordModeChanged = false
        PhotoManager.shared.setPhotos(myPhotos: myPhotos)
        //проверка, можно удалить
        PhotoManager.shared.printUserChoise()
        sideMenuViewController = self.storyboard?.instantiateViewController(withIdentifier: "SideMenuViewController") as! SideMenuViewController
    
        photoCollectionView.contentInset = UIEdgeInsets(top: PhotoCollectionInsetConstraints.verticalConstraint, left: PhotoCollectionInsetConstraints.sideConstraint, bottom: PhotoCollectionInsetConstraints.verticalConstraint, right: PhotoCollectionInsetConstraints.sideConstraint)
        //swipe на закрытие меню
        let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(self.closeSideMenuSwipe))
        swipeLeft.direction = .left
        self.view.addGestureRecognizer(swipeLeft)
        
    }
    
    @IBAction func togleMenuButtonPressed(_ sender: UIButton) {
        if AppDelegate.isSideMenuViewController {
            showSideMenu()
        } else {
            hideSideMenu()
        }
    }
    
    
    @IBAction func enableEditingButtonPressed(_ sender: UIButton) {
        isEditingEnabled = !isEditingEnabled
    }
    
    @IBAction func addImageButtonPressed(_ sender: UIButton) {
        
        let alert = UIAlertController(title: "Add photo", message: "Chose you photo source", preferredStyle: .actionSheet)
        let cameraButton = UIAlertAction(title: "Open camera", style: .default) { (_) in
            self.imagePicker.sourceType = .camera
            self.present(self.imagePicker, animated: true, completion: nil)
        }
        let photoLibraryButton = UIAlertAction(title: "Open photo library", style: .default) { (_) in
            self.imagePicker.sourceType = .photoLibrary
            self.present(self.imagePicker, animated: true, completion: nil)
        }
        let cancelButton = UIAlertAction(title: "Quit", style: .cancel, handler: nil)
        alert.addAction(cameraButton)
        alert.addAction(photoLibraryButton)
        alert.addAction(cancelButton)
        self.present(alert, animated: true, completion: nil)
    }
    
    func showSideMenu(){
        UIView.animate(withDuration: 0.3) {
            self.sideMenuViewController.view.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height)
            self.addChild(self.sideMenuViewController)
            self.view.addSubview(self.sideMenuViewController.view)
            AppDelegate.isSideMenuViewController = false
        }
    }
    
    func hideSideMenu(){
        if !AppDelegate.isSideMenuViewController {
            UIView.animate(withDuration: 0.3, animations: {
                self.sideMenuViewController.view.frame = CGRect(x: -UIScreen.main.bounds.size.width, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height)
            }) { (finished) in
                self.sideMenuViewController.view.removeFromSuperview()
                AppDelegate.isSideMenuViewController = true
                //проверка можно удалять
                print("изменения были \(PhotoCollectionViewController.isUserPasswordModeChanged)")
                if PhotoCollectionViewController.isUserPasswordModeChanged {
                    self.navigationController?.popToRootViewController(animated: false)
                }
            }
        }
    }
    
    @objc func closeSideMenuSwipe(gesture: UISwipeGestureRecognizer){
        hideSideMenu()
    }
    
    
    
}



