//
//  PasswordViewController.swift
//  PhotoStorage
//
//  Created by georg syncov on 07/03/2019.
//  Copyright © 2019 georg syncov. All rights reserved.
//

import UIKit

class PasswordViewController: UIViewController {
    
    @IBOutlet weak var backgroundImageView: UIImageView!
    var userPassword: UITextField?
    var repeteUserPassword: UITextField?
    var newPassword: UITextField?
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.userPassword?.delegate = self
        self.repeteUserPassword?.delegate = self
        self.newPassword?.delegate = self
        backgroundImageView.image = UIImage(named: "mona_lisa")
        backgroundImageView.contentMode = .scaleAspectFill
        addParallaxToView(view: backgroundImageView, magnitude: 30)
        if PasswordManager.shared.getRequestForChangePassword() && PasswordManager.shared.getUserPassword() != nil{
            newPasswordSetAlert()
        } else {
            autorisationUser()
        }
    }
    
    func autorisationUser(){
          print("start autorisation")
        //первый запуск - пароля нет, но по умолчанию он нужен - приложение предложит создать пароль, если пользователь выберет продолжить без ввода пароля - при следующих запусках не будет спрашивать, можно изменить в настройках приложения
        
        let isPasswordNeedet = PasswordManager.shared.getPasswordNeedet() ?? true
        print(isPasswordNeedet)
        //поиск установленного пароля
        //если пароль установлен и он нужен
        if PasswordManager.shared.getUserPassword() != nil && isPasswordNeedet {
            //проверка времени с момента выхода из приложения на превышение timeInterval
            print("время превышено \(UserSettings.shared.isUserTimeIntervalForPasswordRequestExceeded())")
            passwordCheckAlert()
            //если пароль не установлен и пользователь выбрал режим без пароля - переходим с уровнем public, если выбран режим с паролем - запрос на установку пароля
        } else {
            if isPasswordNeedet {
                //пароль нужен и не установлен
                //алерт установки пароля
                passwordSetAlert()
            } else {
                //пароль не нужен
                PhotoManager.shared.setUserChoise(check: false)
                goToPhotoCollectionViewController()
            }
            
        }
        
    }
    //алерт проверки пароля - одно TF
    func passwordCheckAlert(){
        let alert = UIAlertController(title: "Sign in Photo Storage", message: nil, preferredStyle: .alert)
        alert.addTextField(configurationHandler: userPassword)
        let okAcktion = UIAlertAction(title: "Login in", style: .default, handler: self.passwordCheck)
        let continueButton = UIAlertAction(title: "Continue without entering password", style: .cancel, handler: self.continueWithoutPassword)
        alert.addAction(okAcktion)
        alert.addAction(continueButton)
        self.present(alert, animated: true, completion: nil)
        
    }
    //алерт установки пароля - 2 TF
    func passwordSetAlert(){
        let alert = UIAlertController(title: "Set password", message: nil, preferredStyle: .alert)
        alert.addTextField(configurationHandler: userPassword)
        alert.addTextField(configurationHandler: repeteUserPassword)
        
        let okAcktion = UIAlertAction(title: "Ok", style: .default, handler: self.setUserPassword)
        let continueButton = UIAlertAction(title: "Continue without setting password", style: .cancel, handler: self.continueWithoutPassword)
        alert.addAction(okAcktion)
        alert.addAction(continueButton)
        self.present(alert, animated: true, completion: nil)
        
    }
    
    //алерт смены пароля - 3 TF
    func newPasswordSetAlert(){
        let alert = UIAlertController(title: "Set new password", message: nil, preferredStyle: .alert)
        alert.addTextField(configurationHandler: userPassword)
        alert.addTextField(configurationHandler: newPassword)
        alert.addTextField(configurationHandler: repeteUserPassword)
        
        let okAcktion = UIAlertAction(title: "Set new password", style: .default, handler: self.setNewUserPassword)
        let continueButton = UIAlertAction(title: "Continue without setting password", style: .cancel, handler: self.continueWithoutPassword)
        alert.addAction(okAcktion)
        alert.addAction(continueButton)
        self.present(alert, animated: true, completion: nil)
        
    }
    
    //алерт на неправильно введенный пароль при установке
    func incorrectPasswordAlert(){
        let alert = UIAlertController(title: "You password is incorrect", message: "The password must contain at least 6 characters and match in the 2 input fields. Try again.", preferredStyle: .alert)
        
        let okAcktion = UIAlertAction(title: "OK", style: .default, handler: self.tryAgain)
        
        alert.addAction(okAcktion)
        self.present(alert, animated: true, completion: nil)
        
    }
    func incorrectNewPasswordAlert(title: String, message: String){
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        let okAcktion = UIAlertAction(title: "OK", style: .default, handler: self.tryAgainSetNewPassword)
        
        alert.addAction(okAcktion)
        self.present(alert, animated: true, completion: nil)
        
    }
    
    //TF для алертов запроса и установки пароля, ввода нового пароля
    func userPassword(textField: UITextField!) {
        userPassword = textField
        userPassword?.placeholder = "Enter password"
        userPassword?.isSecureTextEntry = true
        self.userPassword?.delegate = self
    }
    func repeteUserPassword(textField: UITextField!) {
        repeteUserPassword = textField
        repeteUserPassword?.placeholder = "Repete password"
        repeteUserPassword?.isSecureTextEntry = true
        self.repeteUserPassword?.delegate = self
    }
    
    func newPassword(textField: UITextField!) {
        newPassword = textField
        newPassword?.placeholder = "Enter new password"
        newPassword?.isSecureTextEntry = true
        self.newPassword?.delegate = self
    }
    //проверка пароля если правильный переходим и установливаем userChoise в секрет, если неправильно - переходим и userChoise в public
    func passwordCheck(alert: UIAlertAction){
        guard let etalonPassword = PasswordManager.shared.getUserPassword() else {
            print("error password not setup")
            return}
        if let text = userPassword?.text {
            PhotoManager.shared.setUserChoise(check: text.elementsEqual(etalonPassword))
            if text.elementsEqual(etalonPassword) {
                print("password correct")
            } else {
                print("password error")
            }
        }
        goToPhotoCollectionViewController()
    }
    //выбор пользователя - переход без пароля - уровень public и перешли
    func continueWithoutPassword(alert: UIAlertAction) {
        PasswordManager.shared.setPasswordNeedet(isNeedet: false)
        PhotoManager.shared.setUserChoise(check: false)
        goToPhotoCollectionViewController()
        
    }
    //проверка пароля при установке и сохранение его
    func setUserPassword(alert: UIAlertAction) {
        print("проверка паролей")
        let password = userPassword?.text ?? ""
        let repetePassword = repeteUserPassword?.text ?? ""
        if repetePassword.elementsEqual(password){
            if PasswordManager.shared.setUserPassword(password: repetePassword){
                PhotoManager.shared.setUserChoise(check: true)
                PasswordManager.shared.setPasswordNeedet(isNeedet: true)
                goToPhotoCollectionViewController()
            }
        } else {
            print("пароли введены неправильно")
            incorrectPasswordAlert()
        }
        
    }
    
    //проверка пароля при замене и сохранение его
    func setNewUserPassword(alert: UIAlertAction) {
        var title: String?
        var message: String?
        print("установка нового пароля")
        let oldPassword = userPassword?.text ?? ""
        let newPassword = self.newPassword?.text ?? ""
        let repetePassword = repeteUserPassword?.text ?? ""
        if let etalonPassword = PasswordManager.shared.getUserPassword() {
            if oldPassword.elementsEqual(etalonPassword){
                if repetePassword.elementsEqual(newPassword){
                    if PasswordManager.shared.setUserPassword(password: newPassword){
                        PasswordManager.shared.isPasswordNeedet = true
                        PhotoManager.shared.setUserChoise(check: true)
                        goToPhotoCollectionViewController()} else {
                        title = "ERROR"
                        message = "Passwords is empty"
                    }
                } else {
                    title = "ERROR"
                    message = "Passwords do not match"
                }
            } else {
                title = "ERROR"
                message = "Incorect old password"
            }
        }
        incorrectNewPasswordAlert(title: title ?? "", message: message ?? "")
    }
    
    //дейстивие для ошибки при установке пароля
    func tryAgain(alert: UIAlertAction){
        passwordSetAlert()
    }
    
    func tryAgainSetNewPassword(alert: UIAlertAction){
        newPasswordSetAlert()
    }
    
    //переход на представление фото коллекции
    func goToPhotoCollectionViewController(){
        PasswordManager.shared.setRequestForChangePassword(value: false)
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "PhotoCollectionViewController") as! PhotoCollectionViewController
        self.navigationController?.pushViewController(controller, animated: false)
    }
    
    
    
    func addParallaxToView(view: UIView, magnitude: Float) {
        let horizontal = UIInterpolatingMotionEffect(keyPath: "center.x", type: .tiltAlongHorizontalAxis)
        horizontal.minimumRelativeValue = -magnitude
        horizontal.maximumRelativeValue = magnitude
        
        let vertical = UIInterpolatingMotionEffect(keyPath: "center.y", type: .tiltAlongVerticalAxis)
        vertical.minimumRelativeValue = -magnitude
        vertical.maximumRelativeValue = magnitude
        
        let group = UIMotionEffectGroup()
        group.motionEffects = [horizontal, vertical]
        view.addMotionEffect(group)
    }
    

}

extension UIAlertController: UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
}

extension PasswordViewController: UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
}
