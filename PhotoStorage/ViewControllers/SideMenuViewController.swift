//
//  SideMenuViewController.swift
//  PhotoStorage
//
//  Created by georg syncov on 11/03/2019.
//  Copyright © 2019 georg syncov. All rights reserved.
//

import UIKit

class SideMenuViewController: UIViewController {
    @IBOutlet weak var passwordModeSwitch: UISwitch!
    
    @IBOutlet weak var passwordModeLabel: UILabel!
    @IBOutlet weak var timeIntervalForRequesPassword: UILabel!
    @IBOutlet weak var userModeButton: MainButton!
    @IBOutlet weak var timeIntervalSlider: UISlider!
    
    @IBOutlet weak var setNewPasswordButton: MainButton!
    var isPasswordNeedet: Bool! {
        didSet {
            
            if isPasswordNeedet{
                passwordModeLabel.text = "Password enabled"
                passwordModeLabel.textAlignment = .center
                passwordModeLabel.backgroundColor = .green
           
            } else {
                passwordModeLabel.text = "Password disabled"
                 passwordModeLabel.textAlignment = .center
                passwordModeLabel.backgroundColor = .red
            }
            
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.view.backgroundColor = .clear
        isPasswordNeedet = PasswordManager.shared.getPasswordNeedet()
        passwordModeSwitch.isOn = isPasswordNeedet
        timeIntervalSlider.value = Float(UserSettings.shared.getUserTimeIntervalForPasswordRequest())
        timeIntervalForRequesPassword.text = "Password request timeinterval is \(UserSettings.shared.getUserTimeIntervalForPasswordRequest()) min"
    }
    
    
    
    
    @IBAction func timeIntervalSlider(_ sender: UISlider) {
        timeIntervalForRequesPassword.text = "Password request timeinterval is \(String(Int(sender.value))) min"
        UserSettings.shared.setUserTimeIntervalForPasswordRequest(timeInterval: Int(sender.value))
    }
    @IBAction func passwordModeSwitchTap(_ sender: UISwitch) {
        isPasswordNeedet = !isPasswordNeedet
        PasswordManager.shared.setPasswordNeedet(isNeedet: isPasswordNeedet)
        PhotoCollectionViewController.isUserPasswordModeChanged = !PhotoCollectionViewController.isUserPasswordModeChanged
    }
    
    @IBAction func setNewPasswordButtonPressed(_ sender: UIButton) {
        PasswordManager.shared.setRequestForChangePassword(value: true)
          AppDelegate.isSideMenuViewController = true
        self.navigationController?.popToRootViewController(animated: false)
    }
    
    
    
}
