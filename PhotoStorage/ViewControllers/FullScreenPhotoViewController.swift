


import UIKit

class FullScreenPhotoViewController: UIViewController, UIScrollViewDelegate {
    
    @IBOutlet weak var scrollViewOnFullScreen: UIScrollView!{
        didSet{
            scrollViewOnFullScreen.minimumZoomScale = 1/25
            scrollViewOnFullScreen.maximumZoomScale = 2.5
            scrollViewOnFullScreen.delegate = self
            }
    }
    
    @IBOutlet weak var customNavBarOnFullScreen: UIView!
    @IBOutlet weak var backToPhotoCollectBut: UIButton!
    
    
    var fullScreenPhotoImageView = UIImageView()
    var image = UIImage()
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupImage(image)
    }
    
    func setupImage(_ image: UIImage) {
        fullScreenPhotoImageView.image = image
        fullScreenPhotoImageView.contentMode = .scaleAspectFit
        fullScreenPhotoImageView.frame.size = CGSize(width: self.view.frame.width, height: self.view.frame.height - customNavBarOnFullScreen.frame.height)
        scrollViewOnFullScreen.contentSize = fullScreenPhotoImageView.frame.size
        fullScreenPhotoImageView.center = scrollViewOnFullScreen.center
        scrollViewOnFullScreen.addSubview(fullScreenPhotoImageView)
    }
    
    @IBAction func backToPhotoCollectButPressed(_ sender: UIButton) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return fullScreenPhotoImageView
    }
    
}


