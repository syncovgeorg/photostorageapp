
protocol EditingViewControllerDelegate {
    func updateUserPhotos(_: [Int: MyUserData])
    func removeUserPhoto(_: Int)
}

import UIKit

class EditingViewController: UIViewController {
    
    var userDataForEditing = [Int: MyUserData]()
    var index: Int?
    
    @IBOutlet weak var bottomLayoutConstraint: NSLayoutConstraint!
    @IBOutlet weak var scrollViewEditingController: UIScrollView!
    @IBOutlet weak var navBarEditingController: UIView!
    @IBOutlet weak var backButtonEditingController: UIButton!
    
    @IBOutlet weak var imageViewOnEditionViewController: UIImageView!
    
    @IBOutlet weak var editingDescriptionTextField: UITextField!
    
    var delegate : EditingViewControllerDelegate?
    
    
//    override func viewDidLoad() {
//        super.viewDidLoad()
//        
//    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.editingDescriptionTextField.delegate = self
        setEditingView(userDataForEditing)
        registerForKeyboardNotifications()
    }
    
    func setEditingView(_ userDataForEditing: [Int:MyUserData] ) {
        guard let key = userDataForEditing.first?.key else {
            return
        }
        let myUserData = userDataForEditing[key]
        imageViewOnEditionViewController.image = myUserData?.getMyUserImage()
        imageViewOnEditionViewController.contentMode = .scaleAspectFit
        editingDescriptionTextField.text = myUserData?.getMyUserDescription()
        editingDescriptionTextField.borderStyle = .line
        
    }
    
    func makeEditingUserData(){
        guard let key = userDataForEditing.first?.key else {
            return
        }
        guard let image = imageViewOnEditionViewController.image else {
            return
        }
        guard let description = editingDescriptionTextField.text else {
            return
        }
        let myUserUpdateData = [key: MyUserData(myUserImage: image, myUserDescription: description)]
        delegate?.updateUserPhotos(myUserUpdateData)
        
    }
    
    func returnIndexOfDeletedPhoto(){
        guard let index = userDataForEditing.first?.key else {
            return
        }
        delegate?.removeUserPhoto(index)
    }
    
   
    
    
    @IBAction func backButtonEditingControllerPressed(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
        makeEditingUserData()
    }
    
    @IBAction func deleteButtonPressed(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
        returnIndexOfDeletedPhoto()
    }
    
    private func registerForKeyboardNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(_:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(_:)), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    
    
    @objc private func keyboardWillShow(_ notification: NSNotification) {
        let userInfo = notification.userInfo!
        let animationDuration = (userInfo[UIResponder.keyboardAnimationDurationUserInfoKey] as! NSNumber).doubleValue
        let keyboardScreenEndFrame = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        
        if notification.name == UIResponder.keyboardWillHideNotification {
            bottomLayoutConstraint.constant = 0
        } else {
            bottomLayoutConstraint.constant = keyboardScreenEndFrame.height
        }
        
        view.needsUpdateConstraints()
        
        UIView.animate(withDuration: animationDuration) {
            self.view.layoutIfNeeded()
        }
    }
    
   

    
    
}

extension EditingViewController:UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
}

