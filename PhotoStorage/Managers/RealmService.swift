//
//  RealmService.swift
//  PhotoStorage
//
//  Created by georg syncov on 10/04/2019.
//  Copyright © 2019 georg syncov. All rights reserved.
//

import Foundation
import RealmSwift


class RealmService{
    
    static let sharedInstance = RealmService()
    private init(){}
    var realm = try! Realm()
    
    func createRealmObj<T: Object>(_ object: T){
        do{
            try realm.write{
                realm.add(object)
            }
        }catch{
            post(error)
        }
    }
    func updateRealmObj<T: Object>(_ object: T, with dictionary : [String: Any?]){
        do{
            try realm.write{
                for (key,val) in dictionary{
                    object.setValue(val, forKey: key)
                }
            }
        }catch{
            post(error)
        }
    }
    func delRealmObj<T: Object>(_ object: T){
        do{
            try realm.write{
                realm.delete(object)
            }
        }catch{
            post(error)
        }
    }
    func post(_ error: Error){
        NotificationCenter.default.post(name: NSNotification.Name("RealmError"), object: error)
    }
    func observeRealmErrors(in vc: UIViewController, completion: @escaping (Error?) -> Void){
        NotificationCenter.default.addObserver(forName: NSNotification.Name("RealmError")
        , object: nil, queue: nil) { (notification) in
            completion(notification.object as? Error)
        }
    }
}
