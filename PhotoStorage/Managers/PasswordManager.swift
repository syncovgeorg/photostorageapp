//
//  PasswordManager.swift
//  PhotoStorage
//
//  Created by georg syncov on 07/03/2019.
//  Copyright © 2019 georg syncov. All rights reserved.
//

import Foundation
import UIKit
import Locksmith

class PasswordManager {
    
    static let shared = PasswordManager()
    private init(){}
    let defaults = UserDefaults.standard
    
    
    var requestForChangePassword = false
    var userPassword: String?
    var userTimeIntervalForPasswordRequest: Int?
    
    var isUserAuthorized: Bool?
    var isPasswordNeedet: Bool? {
        didSet {
            defaults.set(isPasswordNeedet, forKey: "isPasswordNeedet")
        }
    }
    
    func setRequestForChangePassword(value: Bool){
        requestForChangePassword = value
    }
    
    func getRequestForChangePassword() -> Bool{
        return requestForChangePassword
    }
    
    
    func setUserTimeIntervalForPasswordRequest(value: Int){
        self .userTimeIntervalForPasswordRequest = value
    }
    
    
    func setPasswordNeedet(isNeedet: Bool) {
        isPasswordNeedet = isNeedet
    }
    
    func getPasswordNeedet()-> Bool?{
        return defaults.bool(forKey: "isPasswordNeedet")
    }
    
    
    
    func setUserPassword(password: String) -> Bool{
        var result = false
        if password.isCorrect {
            do {
                try Locksmith.saveData(data: ["password": password], forUserAccount: "myUser")
                result = true
            }
            catch {
                print("dont save password")
            }
        }
        return result
    }
    
    func getUserPassword() -> String? {
        guard let dict = Locksmith.loadDataForUserAccount(userAccount: "myUser") else {return nil}
        return dict["password"] as? String
    }
    
    func changePassword(newValue: String) {
        if newValue.isCorrect{
            do{
                try? Locksmith.updateData(data: ["password": newValue], forUserAccount: "myUser")
            }
            catch {
                print("dont change password")
            }
        }
    }
    
    
}

extension String {
    var isCorrect: Bool {
        if self.count >= 6 {
            return true
        } else {
            return false
        }
    }
    
}

