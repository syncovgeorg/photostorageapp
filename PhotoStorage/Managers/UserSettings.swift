//
//  UserSettings.swift
//  PhotoStorage
//
//  Created by georg syncov on 12/03/2019.
//  Copyright © 2019 georg syncov. All rights reserved.
//

import Foundation
import UIKit

struct PhotoCollectionInsetConstraints {
    static let verticalConstraint: CGFloat = 10
    static let sideConstraint: CGFloat = 5
}

class UserSettings {
    
    static let shared = UserSettings()
    private init() {}
    
    var userTimeIntervalForPasswordRequest: Int = 5
    
   
    
    var timeFineshedApp: Int?
    
    let defaults = UserDefaults.standard
    
    
    
    func setTimeFinishedApp(){
        let dateNow = Int(Date().timeIntervalSinceReferenceDate) / 60
        defaults.set(dateNow, forKey: "timeFineshedApp")
    }
    func getTimeFinishedApp()->Int?{
        return defaults.integer(forKey: "timeFineshedApp")
    }
    
    func isUserTimeIntervalForPasswordRequestExceeded()->Bool{
        var result = true
        let dateNow = Int(Date().timeIntervalSinceReferenceDate) / 60
        if let dateFinishedApp = getTimeFinishedApp(){
            let timeInterval = dateNow - dateFinishedApp
            if timeInterval < getUserTimeIntervalForPasswordRequest(){
                result = false
            }
        }
        return result
    }
    
    
    func getUserTimeIntervalForPasswordRequest()->Int {
        return defaults.integer(forKey: "userTimeIntervalForPasswordRequest")
    }
    
    func setUserTimeIntervalForPasswordRequest(timeInterval: Int) {
         defaults.set(timeInterval, forKey: "userTimeIntervalForPasswordRequest")
    }
    
    
    
}
