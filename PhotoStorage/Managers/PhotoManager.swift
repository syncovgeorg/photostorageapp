//
//  PhotoManager.swift
//  PhotoStorage
//
//  Created by georg syncov on 01/03/2019.
//  Copyright © 2019 georg syncov. All rights reserved.
//

import Foundation
import RealmSwift

enum UserChoise : String {
    case secretUser = "secret"
    case publicUser = "public"
}

class PhotoManager {
    static let shared = PhotoManager()
    let defaults = UserDefaults.standard
    private init() {}
    
    var userChoise: UserChoise?{
        didSet {
            defaults.set(userChoise?.rawValue, forKey: "UserChoise")
        }
    }
    
    var myUserData = [MyUserData]()
    
    func setUserChoise(check: Bool){
        if check{
            userChoise = UserChoise.secretUser
        } else {
            userChoise = UserChoise.publicUser
        }
    }
    
    func getUserChoise() ->String{
        return defaults.string(forKey: "UserChoise") ?? "public"
    }
    
    
    func getPhotos() -> [MyUserData]? {
        let key = defaults.string(forKey: "UserChoise") ?? "public"
        guard let data = defaults.object(forKey: key) as? Data else {return nil}
        let myUserData = try? JSONDecoder().decode([MyUserData].self, from: data)
        return myUserData
    }
    
    func setPhotos(myPhotos: [MyUserData]) {
        let key = defaults.string(forKey: "UserChoise") ?? "public"
             if let data = try?JSONEncoder().encode(myPhotos){
                defaults.set(data, forKey: key)
            }
    }
    
    func addNewUserData(_ userData: MyUserData) {
        let key = defaults.string(forKey: "UserChoise") ?? "public"
        if var myUserData = getPhotos()
        {
            myUserData.append(userData)
            if let data = try? JSONEncoder().encode(myUserData) {
                defaults.set(data, forKey: key)
            }
        } else {
            var myUserData = [MyUserData]()
            myUserData.append(userData)
            
            if let data = try? JSONEncoder().encode(myUserData) {
                defaults.set(data, forKey: key)
            }
        }
    }
    
}
