//
//  MainButton.swift
//  PhotoStorage
//
//  Created by georg syncov on 06/03/2019.
//  Copyright © 2019 georg syncov. All rights reserved.
//

import UIKit

class MainButton: UIButton {

    override func didMoveToWindow() {
        self.layer.cornerRadius = self.frame.height / 3
        self.setTitleColor(UIColor.orange, for: .highlighted)
        self.layer.shadowRadius = 9
        self.layer.shadowOpacity = 0.3
        self.layer.shadowOffset = CGSize(width: 5, height: 8)
        self.clipsToBounds = false
        
    }

}
