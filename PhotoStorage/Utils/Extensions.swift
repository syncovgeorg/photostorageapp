//
//  Extensions.swift
//  PhotoStorage
//
//  Created by georg syncov on 06/03/2019.
//  Copyright © 2019 georg syncov. All rights reserved.
//

import Foundation
import UIKit

extension PhotoCollectionViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout  {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        //return myTestPhotos.count
        return myPhotos.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "photoCell", for: indexPath) as? PhotoCollectionViewCell
            else {
                return UICollectionViewCell()
        }
        //cell.setUserDataOnPhotoView(myTestPhotos[indexPath.row])
        cell.setUserDataOnPhotoView(myPhotos[indexPath.row])
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let size = CGSize(width: (self.view.frame.width / 2) - 10, height: 150)
        
        
        return size
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if isEditingEnabled {
            
            //let myUserDataForEditing = [indexPath.row : myTestPhotos[indexPath.row]]
            let myUserDataForEditing = [indexPath.row : myPhotos[indexPath.row]]
            let controller = self.storyboard?.instantiateViewController(withIdentifier: "EditingViewController") as! EditingViewController
            controller.userDataForEditing = myUserDataForEditing
            controller.delegate = self
            self.navigationController?.pushViewController(controller, animated: true)
        } else {
            //            guard let image = myTestPhotos[indexPath.row].getMyUserImage() else {return}
            guard let image = myPhotos[indexPath.row].getMyUserImage() else {return}
            let controller = self.storyboard?.instantiateViewController(withIdentifier: "FullScreenPhotoViewController") as! FullScreenPhotoViewController
            controller.image = image
            self.navigationController?.pushViewController(controller, animated: true)
            
        }
    }
    
    
}

extension PhotoCollectionViewController: EditingViewControllerDelegate {
    
    func updateUserPhotos(_ myUserDataDict: [Int : MyUserData]) {
        
        guard let myIndex = myUserDataDict.first?.key else {
            return
        }
        guard let myUserData = myUserDataDict[myIndex] else {
            return
        }
        myPhotos[myIndex].setMyUserDescription(myUserData.getMyUserDescription() ?? "")
        photoCollectionView.reloadData()
        
    }
    
    func removeUserPhoto(_ index: Int) {
        myPhotos.remove(at: index)
        photoCollectionView.reloadData()
    }
    
}

extension PhotoCollectionViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let pickerImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            let userData = MyUserData(myUserImage: pickerImage, myUserDescription: "")
            //self.myTestPhotos.append(userData)
            self.myPhotos.append(userData)
             photoCollectionView.reloadData()
            //проверка
            print("add new userData")
            
        }
        dismiss(animated: true, completion: nil)
    }
    
}


