//
//  DescriptionLabel.swift
//  PhotoStorage
//
//  Created by georg syncov on 03/03/2019.
//  Copyright © 2019 georg syncov. All rights reserved.
//

import UIKit

class DescriptionLabel: UILabel {
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initializeLabel()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initializeLabel()
    }
    
    func initializeLabel() {
        
        self.textColor = .red
        self.textAlignment = .center
        self.numberOfLines = 0
        self.lineBreakMode = .byWordWrapping
        self.sizeToFit()
        self.font = UIFont(name: "verdana", size: 15)
        
    }
    
}
